import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ecomm_outlier_detection",
    version="0.1",
    author="David Rodrigues",
    author_email="david.rodrigues@nielsen.com",
    description="Flag and correct outliers",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['ecomm_outlier_detection'],
    install_requires = [
            'pandas',
		    'numpy',
		    'tqdm'
            ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6'
)