import os
import pandas as pd
import numpy as np
from tqdm import tqdm


def _value_correction(df_agg, 
                       userid_col='User_id',
                      uservalue_col='Item_Value',
                      no_of_iterations=2,
                      min_sample=5, print_variables=False):
    """
    Flags & corrects outliers on each period x category subset of the df_agg data
    Default is 2 iterations with no printed outputs and minimum required samples per CategoryXPeriod is 5    
    """
    # Make a list of the iteration numbers in string format 
    iterations = [str(i) for i in range(1,no_of_iterations+1)]

    for i in iterations:

        if print_variables:
            print('\nIteration No:', i)

        # String version of the next iteration number 
        j=str(int(i)+1)

        # Count the number of users for each Period X Category
        CountUsers = df_agg[userid_col].count()

        # Calculate the maximum allowable contribution of each user to a Period X Category bucket. Calculated as 1/Sqrt(n)
        ModContr = pow(CountUsers,-0.5)

        # Flag the users based on the Value contribution 
        TotalValue = df_agg['UserValue'+i].sum()
        df_agg['Contribution'+i] = df_agg['UserValue'+i]/df_agg['UserValue'+i].sum()
        df_agg['OutlierFlag'+i] = np.where(df_agg['Contribution'+i] > ModContr,1, 0)

        # Calculate the sum of Value of the Outliers & Non Outliers
        TotalOutliers = df_agg.loc[df_agg['OutlierFlag'+i]==1,'UserValue'+i].sum()
        TotalNonOutliers = df_agg.loc[df_agg['OutlierFlag'+i]==0,'UserValue'+i].sum()

        # Calculate a new Adjusted Total value (Period x Category) based on the Non Outliers 
        AdjustedTotal = (TotalNonOutliers/(1 - ModContr))
        Diff = (AdjustedTotal - TotalNonOutliers)* 0.9

        # Print 
        if print_variables:
            print('CountUsers:',CountUsers,
                  '\nModContr:',ModContr,
                  '\nTotalValue:',TotalValue,
                  '\nTotalOutliers:',TotalOutliers,
                  '\nTotalNonOutliers:',TotalNonOutliers,
                  '\nAdjustedTotal:',AdjustedTotal,
                  '\nDiff:',Diff)

        # Calculate the value proportion of the Outliers  
        df_agg['OutliersProportion'+i] = np.where(df_agg['OutlierFlag'+i]==1, 
                                                      df_agg['UserValue'+i]/ TotalOutliers,
                                                      0)

        # Calculate new User Values in the same dataframe based on the proportion of the Outliers(in case of multiple outliers)
        # If the users are less than min_sample, User values stay the same 
        if len(df_agg[userid_col].unique()) < min_sample:
            df_agg['UserValue'+j] = df_agg['UserValue'+i]
        else:
            df_agg['UserValue'+j] = np.where(df_agg['OutlierFlag'+i]==1,
                                                 df_agg['OutliersProportion'+i] * Diff,
                                                 df_agg['UserValue'+i])


    # Count the No of corrective iterations on the data 
    df_agg['NoOfIterationsValue'] = df_agg[df_agg.columns[df_agg.columns.str.startswith('OutlierFlag')]].sum(axis=1)

    return df_agg




def quantity_correction(df, period_col='Periodcode',
                        price_col = 'Price',
                        cat_col='Nielsen_category', 
                        userid_col='User_id',
                        uservalue_col='Item_Value',
                        userquantity_col='Quantity_purchased',
                        no_of_iterations=2,
                        min_sample=5):
    """
    Calculates the corrected user level outlier values, 
    and applies the correction factor to the quantity of the flagged users 
    """
        
    # --------------------------------------------------------------------------
    # Aggregate the Price and Quantity by Period, Category & UserID
    # Renames the Value per user & Quantity per User 
    # --------------------------------------------------------------------------
    df_agg = (
        df[[period_col, cat_col, userid_col, uservalue_col, userquantity_col]]
        .groupby([period_col, cat_col, userid_col],as_index=False)
        .agg({uservalue_col:'sum',userquantity_col:'sum'})
        .rename(columns={uservalue_col:'UserValue1',userquantity_col:'UserQuantity1'})
    )
    

    # --------------------------------------------------------------------------
    # Apply _value_correction to the whole df_agg using the default 2 iterations 
    # at minimum sample of 5 per subset
    # --------------------------------------------------------------------------
    tqdm.pandas() 
    
    df_agg = df_agg.groupby([period_col,cat_col]).progress_apply(_value_correction,
                                                                           userid_col=userid_col, 
                                                                           uservalue_col=uservalue_col, 
                                                                           no_of_iterations=no_of_iterations, 
                                                                           min_sample=min_sample)

    # Calculate the correction factor for the flagged Value
    df_agg['ValueCorrectionFactor'] = df_agg['UserValue'+str(no_of_iterations+1)] / df_agg['UserValue1']

    # Merge the df_agg dataframe with the original df dataframe
    corrected_df = pd.merge(df,df_agg, how='left')

    # Correct the price by multiplying Item_Value with the correction factor
    corrected_df['CorrectedItemValue'] = corrected_df[uservalue_col] * corrected_df['ValueCorrectionFactor']

    # Correct the quantity by dividing the Corrected Price by Item Price
    corrected_df['CorrectedQuantity'] = corrected_df['CorrectedItemValue'] / corrected_df[price_col]
    
    return corrected_df
